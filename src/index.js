import http from 'http';
import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import middleware from './middleware';
import config from './config.json';
import db from './db';

let app = express();
app.server = http.createServer(app);

// logger
app.use(morgan('dev'));

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json({limit : config.bodyLimit}));

app.use(cors({exposedHeaders: config.corsHeaders})); 

//Test Database 
db.authenticate()
	.then(() => console.log('Database Connected!!!'))
	.catch(err => console.log('ERR:'+err))

app.get('/', (req, res) => {res.send('Index Page');});
app.use('/api/projects', require('./api/projects'));

app.server.listen(process.env.PORT || config.port, () => {
	console.log(`Started on port ${app.server.address().port}`);
});

export default app;
