const Sequelize = require('sequelize');
import db from '../db';

const Project = db.define('Project', {
    geography: {
        type: Sequelize.STRING,
        defaultValue: "Me"
    },
    client: {
        type: Sequelize.STRING
    },
    name: {
        type: Sequelize.STRING
    },
    startDate: {
        type: Sequelize.DATEONLY
    },
    endDate: {
        type: Sequelize.DATEONLY
    }
})
module.exports = Project;