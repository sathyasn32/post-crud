import express from 'express';
const router = express.Router();
import db from '../db';
import Project from '../models/Project';

router.get('/', (req, res) => 
    Project.findAll()
    .then(projects => res.json(projects))
    .catch(err => console.log(err)));

router.post('/createProject', (req,res) => { 
   let { geography, client, name, startDate, endDate} = req.body;
    Project.create({
        geography,
        client,
        name,
        startDate,
        endDate,
    })
    .then(projects => res.json(projects))
    .catch(err => console.log(err))
});

router.delete('/:id/remove', (req,res) => { 
    const { id } = req.params;
    Project.destroy({ 
        where: { id: id }
    }) 
    .then(() => res.send('project was deleted successfully'))
    .catch(err => comsole.log(err)) 
});

router.put('/:id/update', (req,res) => {
    const { id } = req.params;
    let { geography, client, name, startDate, endDate} = req.body;
    Project.update({
        geography:req.body.geography,
        client:req.body.client,
        name:req.body.name,
        startDate:req.body.startDate,
        endDate:req.body.endDate,
    },
    { where : { id : id }})
    .then(() => res.json('project was updated successfully'))
    .catch(err => console.log(err))
});

module.exports = router;

